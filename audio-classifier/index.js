import * as tf from "@tensorflow/tfjs-node";
import * as fs from "fs";
import * as path from "path";
import * as wav from "node-wav";

// Define the model architecture

const inputSize = 45056;
const numClasses = 2;

const model = tf.sequential();
model.add(
  tf.layers.dense({ units: 64, activation: "relu", inputShape: [inputSize] })
);
model.add(tf.layers.dense({ units: numClasses, activation: "softmax" }));

// Compile the model
model.compile({
  optimizer: "adam",
  loss: "categoricalCrossentropy",
  metrics: ["accuracy"],
});

// Load and preprocess the audio data
const audioFolder = "./audio";
const files = fs.readdirSync(audioFolder);
const audioData = files.map((file) => {
  const filePath = path.join(audioFolder, file);
  const buffer = fs.readFileSync(filePath);
  const result = wav.decode(buffer);
  return result;
});

// Preprocess your audio data here
const preprocessedData = preprocessAudioData(audioData);

// Convert the labels to one-hot encoding
const labels = getLabels(files).map((label) => parseInt(label));
const stringLabels = getLabels(files);
console.log("Labels", labels);
const oneHotLabels = tf.oneHot(labels, numClasses);

// Train the model
model
  .fit(preprocessedData, oneHotLabels, {
    epochs: 10,
    batch_size: 32,
    verbose: 2,
  })
  .then((history) => {
    console.log("Training completed!");
    // Evaluate the model
    const evalResult = model.evaluate(preprocessedData, oneHotLabels);
    console.log(`Accuracy: ${evalResult[1].dataSync()[0]}`);

    // Save the model
    const modelPath = "file://./model";
    model.save(modelPath).then(() => {
      // Save metadata JSON file
      const metadata = {
        tfjsSpeechCommandsVersion: "0.4.0",
        modelName: "TMv2",
        timeStamp: new Date().toISOString(),
        wordLabels: stringLabels,
      };

      const metadataPath = "./model/metadata.json";

      fs.writeFile(metadataPath, JSON.stringify(metadata), (err) => {
        if (err) throw err;
      });
      console.log("Model and metadata saved successfully!");
    });
  })
  .catch((error) => {
    console.error("Error during training:", error);
  });

// Preprocess the audio data
function preprocessAudioData(audioData) {
  // Convert the audio data to tensors
  const tensors = audioData.map((data) => {
    const values = normalize(data.channelData[0]);
    return tf.tensor(values);
  });

  // Pad the tensors to have the same shape
  const paddedTensors = padTensors(tensors);

  // Convert the tensors to a 2d tensor
  const x = tf.stack(paddedTensors);

  // Return the preprocessed data
  return x;
}

// Normalize the audio data
function normalize(data) {
  const min = Math.min(...data);
  const max = Math.max(...data);
  return data.map((value) => (value - min) / (max - min));
}

// Pad the tensors to have the same shape
function padTensors(tensors) {
  const maxLength = tensors.reduce(
    (max, tensor) => Math.max(max, tensor.shape[0]),
    0
  );
  return tensors.map((tensor) => {
    const pad = maxLength - tensor.shape[0];
    return tf.pad(tensor, [[0, pad]]);
  });
}

function getLabels(files) {
  return files.map((file) => {
    const [label] = file.split(".");
    return label;
  });
}
