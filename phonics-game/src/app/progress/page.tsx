"use client";

// DEPENDENCIES //

import React, { useEffect, useState } from "react";

import AuthChecker from "@/app/components/AuthChecker";

import BackButton from "@/app/components/BackButton";
import PlayLevel from "@/app/learn/PlayLevel";

import { levels } from "@/app/learn/questions";
import { useRouter } from "next/navigation";

import { auth, db } from "../firebaseInit";
import { get, child, ref } from "firebase/database";

import { styles } from "@/app/GlobalTailwindStyles";

// COMPONENTS //

export default function Progress() {
  const [level, setLevel] = useState<number>(-1);
  const [loaded, setLoaded] = useState<boolean>(false);

  const router = useRouter();

  const addTimes = (times: number[]) => {
    let total = 0;
    times.forEach((t) => {
      total += t;
    });
    return total;
  };

  const [timesDictionary, setTimesDictionary] = useState();

  useEffect(() => {
    auth.onAuthStateChanged((user) => {
      const dbRef = ref(db);
      get(child(dbRef, `users/${auth.currentUser?.uid}/times`))
        .then((snapshot) => {
          const tmp = snapshot.val();

          setTimesDictionary(tmp);
          console.log(tmp);

          setLoaded(true);
        })
        .catch((error) => {
          console.error(error);
        });
    });
  }, [level, setLevel]);

  return (
    <main>
      <AuthChecker />
      <BackButton
        handlePress={() => {
          if (level > -1) setLevel(-1);
          else router.push("/dashboard");
        }}
      />

      <div className="flex flex-col items-center justify-center min-h-screen p-16">
        {loaded ? (
          <>
            <table className="text-black bg-white rounded-lg shadow-2xl">
              <thead>
                <tr>
                  <th>Level</th>
                  <th>Time</th>
                </tr>
              </thead>
              <tbody>
                {levels.map((level, index) => (
                  <tr key={index}>
                    <td className="p-2">{level.getTitle()}</td>
                    <td>
                      <details className="">
                        <summary className="flex w-full cursor-pointer">
                          <h3 className="">
                            {addTimes(
                              timesDictionary?.[level.getTitle()] || [-1]
                            ) > 0
                              ? "Total Time: " +
                                addTimes(
                                  timesDictionary?.[level.getTitle()] || [-1]
                                ) /
                                  1000 +
                                "s"
                              : "Incomplete"}
                          </h3>
                        </summary>
                        {[]
                          .concat(timesDictionary?.[level.getTitle()] || [])
                          .map((t, tIndex) => (
                            <p
                              key={"question" + tIndex}
                              className={
                                t > 5000
                                  ? "ml-6 text-red-500"
                                  : "ml-6 text-green-500"
                              }
                            >
                              {`${level.getQuestionITitle(tIndex)}: ${(
                                Number(t) / 1000
                              ).toString()}s`}
                            </p>
                          ))}
                      </details>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </>
        ) : (
          <h1 className={styles.h1}>Loading...</h1>
        )}
      </div>
    </main>
  );
}
