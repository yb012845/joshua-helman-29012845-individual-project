// CSS //
import "@/app/globals.css";
import { Inter } from "next/font/google";
import type { Metadata } from "next";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Let's Learn Phonics! | My Progress",
  description: "A game to help children learn to speak.",
  keywords: ["phonics", "learning", "children", "game"],
  authors: [
    {
      name: "Josh Helman",
    },
  ],
  creator: "Josh Helman",
  openGraph: {
    title: "Let's Learn Phonics! | My Progress", // OG Title
    description: "A game to help children learn to speak.", // OG Description
    siteName: "Let's Learn Phonics!", // OG Site Name
    url: "https://fyp-psi-vert.vercel.app", // OG URL
    images: [
      {
        url: `https://fyp-psi-vert.vercel.app/img/icon.png`, // Image for sites to use
        width: 1440,
        height: 720,
      },
    ],
    locale: "en-GB", // Location data
    type: "website", // Type of service
  },
  viewport: "width=device-width, height=device-height, initial-scale=1.0", // Viewport data
};
export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return <main className={inter.className}>{children}</main>;
}
