import { auth } from "@/app/firebaseInit";
import { signOut } from "firebase/auth";
import { useRouter } from "next/navigation";

export default function SignOutButton() {
  const router = useRouter();

  // Handle sign out
  const handleSignOut = () => {
    // Sign out
    signOut(auth)
      // Redirect to login page
      .then(() => {
        router.push("/");
      })
      // Catch error
      .catch((err) => {
        alert(err.message);
      });
  };

  return (
    <div id="sign-out">
      <button
        className="flex p-2 border-solid border border-blue-500 rounded-lg m-4 text-blue-500 transition duration-500 ease-in-out text-center absolute uppercase hover:bg-blue-500 hover:text-white bg-white shadow-xl"
        onClick={handleSignOut}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth="2"
          stroke="currentColor"
          className="flex-shrink-0 w-5 h-5 transition duration-75 text-inherit text-center"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15m3 0l3-3m0 0l-3-3m3 3H9"
          />
        </svg>
        <span className="ml-3">Sign Out</span>
      </button>
    </div>
  );
}
