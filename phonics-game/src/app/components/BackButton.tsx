export default function SignOutButton({
  handlePress,
}: {
  handlePress: () => void;
}) {
  return (
    <div id="back-button">
      <button
        className="flex p-2 border-solid border border-blue-500 rounded-lg m-4 text-blue-500 transition duration-500 ease-in-out text-center absolute uppercase hover:bg-blue-500 hover:text-white bg-white shadow-xl"
        onClick={handlePress} //this is a passed in function to control what the back button does on each page. On some pages it goes back to the dashboard, on others it goes back to the previous page.
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth="2"
          stroke="currentColor"
          className="flex-shrink-0 w-5 h-5 transition duration-75 text-inherit text-center"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M3.24,7.51c-0.146,0.142-0.146,0.381,0,0.523l5.199,5.193c0.234,0.238,0.633,0.064,0.633-0.262v-2.634c0.105-0.007,0.212-0.011,0.321-0.011c2.373,0,4.302,1.91,4.302,4.258c0,0.957-0.33,1.809-1.008,2.602c-0.259,0.307,0.084,0.762,0.451,0.572c2.336-1.195,3.73-3.408,3.73-5.924c0-3.741-3.103-6.783-6.916-6.783c-0.307,0-0.615,0.028-0.881,0.063V2.575c0-0.327-0.398-0.5-0.633-0.261L3.24,7.51 M4.027,7.771l4.301-4.3v2.073c0,0.232,0.21,0.409,0.441,0.366c0.298-0.056,0.746-0.123,1.184-0.123c3.402,0,6.172,2.709,6.172,6.041c0,1.695-0.718,3.24-1.979,4.352c0.193-0.51,0.293-1.045,0.293-1.602c0-2.76-2.266-5-5.046-5c-0.256,0-0.528,0.018-0.747,0.05C8.465,9.653,8.328,9.81,8.328,9.995v2.074L4.027,7.771z"
          />
        </svg>
        <span className="ml-3">{"Back"}</span>
      </button>
    </div>
  );
}
