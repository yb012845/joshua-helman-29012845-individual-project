"use client";

// DEPENDENCIES //

import React, { useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import { auth } from "@/app/firebaseInit";

// COMPONENTS //

/**
 * Checks if the user is logged in and redirects to the login page if not. This should be present on every page that requires authentication.
 */
export default function AuthChecker() {
  // DEPENDENCIES //
  const router = useRouter();

  // EFFECTS //
  useEffect(() => {
    // Check if user is logged in
    auth.onAuthStateChanged((user: any) => {
      // If user is not logged in, redirect to login page
      if (!user) router.push("/");
    });
  }, [router, router.push]);

  return <main></main>;
}
