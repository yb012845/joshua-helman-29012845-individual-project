"use client";

// DEPENDENCIES //

import React, { useEffect, useState } from "react";
import { auth } from "@/app/firebaseInit";
import { useRouter } from "next/navigation";

import { get, child, ref } from "firebase/database";
import { db } from "@/app/firebaseInit";

import SignOutButton from "@/app/components/SignOutButton";
import AuthChecker from "@/app/components/AuthChecker";
import { styles } from "../GlobalTailwindStyles";

import Image from "next/image";

// COMPONENTS //

export default function Dashboard() {
  // DEPENDENCIES //
  const router = useRouter();

  // STATES //
  const [user, setUser] = useState("user");
  const [loaded, setLoaded] = useState(false);

  // EFFECTS //
  useEffect(() => {
    // Check if user is logged in
    auth.onAuthStateChanged((user) => {
      const dbRef = ref(db);
      // Get the user's name from the database
      get(child(dbRef, `users/${auth.currentUser?.uid}/name`))
        .then((snapshot) => {
          if (snapshot.exists()) {
            setUser(snapshot.val());
            setLoaded(true);
          } else {
            console.log("No data available");
          }
        })
        .catch((error) => {
          console.error(error);
        });
    });
  }, [router, router.push]);

  return (
    <main>
      <AuthChecker />
      <SignOutButton />
      <div className="flex flex-col items-center justify-center min-h-screen py-2">
        {loaded ? (
          <>
            <h1 className="text-6xl font-bold text-white">{`Hello, ${user}!`}</h1>
            <p className="mt-3 text-2xl text-white">{`Let's Learn Phonics Together!`}</p>
            <div className="flex flex-wrap items-center justify-around max-w-4xl mt-6 sm:w-full pt-8">
              <a className={styles.cardStyle} href="/learn" title="Play Game">
                <Image
                  src="/img/play_icon.jpg"
                  alt="Play Game"
                  width={1024}
                  height={1024}
                  className="rounded-xl"
                />
              </a>
              <a
                className={styles.cardStyle}
                href="/progress"
                title="My Progress"
              >
                <Image
                  src="/img/progress_icon.jpg"
                  alt="My Progress"
                  width={1024}
                  height={1024}
                  className="rounded-xl"
                />
              </a>
              <a
                className={styles.cardStyle}
                href="/help"
                title="Help and FAQs"
              >
                <Image
                  src="/img/help_icon.jpg"
                  alt="Help and FAQs"
                  width={1024}
                  height={1024}
                  className="rounded-xl"
                />
              </a>
            </div>
          </>
        ) : (
          <h1 className={styles.h1}>Loading...</h1>
        )}
      </div>
    </main>
  );
}
