"use client";

import React, { useState, useEffect } from "react";
import {
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
} from "firebase/auth";
import { auth } from "@/app/firebaseInit";
import { useRouter } from "next/navigation";
import { writeDisplayName } from "./libs/firebaseLibs";

import {
  RegExpMatcher,
  englishDataset,
  englishRecommendedTransformers,
} from "obscenity";

export default function Home() {
  const router = useRouter();

  // STATES //
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const [loggingIn, setLoggingIn] = useState(true);

  const [displayName, setDisplayName] = useState("NEW USER");

  // EFFECTS //
  useEffect(() => {
    // Check if user is logged in
    auth.onAuthStateChanged((user) => {
      if (user) {
        // User is signed in so redirect to dashboard
        router.push("/dashboard");
      }
    });
  }, [router, router.push]);

  // HANDLERS //

  // Handle email input
  const handleEmailChange = (e: {
    target: { value: React.SetStateAction<string> };
  }) => {
    setEmail(e.target.value);
  };

  // Handle password input
  const handlePasswordChange = (e: {
    target: { value: React.SetStateAction<string> };
  }) => {
    setPassword(e.target.value);
  };

  // Handle password input
  const handleConfirmPasswordChange = (e: {
    target: { value: React.SetStateAction<string> };
  }) => {
    setConfirmPassword(e.target.value);
  };

  // Handle sign in
  const handleSignIn = () => {
    // Sign in with email and password
    signInWithEmailAndPassword(auth, email, password)
      // Redirect to dashboard
      .then(() => {
        router.push("/dashboard");
      })
      // Catch error
      .catch((err) => alert(err.message));
  };

  const checkName = () => {
    // Ensures that the username is not undefined or null, and is not an empty string.
    if (!displayName || displayName == "") {
      alert("You must enter a display name.");
      return false;
    }

    // A regular expression declaration for the alphabet.
    let regExAlphabet = /^[a-zA-Z\s-]+$/;

    // Tests if the string fits the regular expression.
    if (!regExAlphabet.test(displayName)) {
      alert("Display name must contain only letters.");
      return false;
    }

    // Creates a regEx matcher
    const matcher = new RegExpMatcher({
      ...englishDataset.build(),
      ...englishRecommendedTransformers,
    });

    // Checks if they rexEx finds a match in the string.
    if (matcher.hasMatch(displayName)) {
      alert("This display name contains profanity. Please use a new name.");
      return false;
    }

    return true;
  };

  // Handle sign up
  const handleSignUp = () => {
    // Validates legal display name
    if (!checkName()) return;

    // Check if passwords don't match
    if (password != confirmPassword) {
      alert("Passwords do not match");
      return;
    }

    // Create user with email and password
    createUserWithEmailAndPassword(auth, email, password)
      // Redirect to dashboard
      .then(() => {
        // Write display name to the database.

        writeDisplayName(auth.currentUser?.uid, displayName);

        router.push("/dashboard");
      })
      // Catch error
      .catch((err) => alert(err.message));
  };

  const handleDisplayName = (e: {
    target: { value: React.SetStateAction<string> };
  }) => {
    setDisplayName(e.target.value);
  };

  return (
    <section className="grid grid-cols-1 gap-2 place-content-center justify-items-center h-screen">
      <h1 className="text-center text-4xl font-bold mb-6">
        {"Let's Learn Phonics"}
      </h1>

      <div className="shadow-xl p-5 w-[600px] bg-white rounded-xl">
        {loggingIn ? (
          <>
            <div className="grid grid-cols-1 gap-4 content-center mb-5 mt-5">
              <div className="grid grid-rows-1 content-center">
                <input
                  className="shadow focus:shadow-lg duration-500 easy-in-out appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none text-center"
                  type="email"
                  placeholder="Email"
                  onChange={handleEmailChange}
                  value={email}
                />
              </div>
              <div className="grid grid-rows-1 content-center">
                <input
                  className="shadow focus:shadow-lg duration-500 easy-in-out appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none text-center"
                  type="password"
                  placeholder="Password"
                  onChange={handlePasswordChange}
                  value={password}
                />
              </div>
            </div>
            <div className="flex flex-col items-center">
              <button
                className="border-solid border rounded-lg border-blue-500 pt-1 pb-1 uppercase pl-24 pr-24 text-blue-500 hover:bg-blue-500 hover:text-white transition duration-500 ease-in-out"
                onClick={handleSignIn}
              >
                Sign In
              </button>
            </div>
          </>
        ) : (
          <>
            <div className="grid grid-cols-1 gap-4 content-center mb-5 mt-5">
              <div className="grid grid-rows-1 content-center">
                <input
                  className="shadow focus:shadow-lg duration-500 easy-in-out appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none text-center"
                  type="email"
                  placeholder="Email"
                  onChange={handleEmailChange}
                  value={email}
                />
              </div>
              <div className="grid grid-rows-1 content-center">
                <input
                  className="shadow focus:shadow-lg duration-500 easy-in-out appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none text-center"
                  type="password"
                  placeholder="Password"
                  onChange={handlePasswordChange}
                  value={password}
                />
              </div>

              <div className="grid grid-rows-1 content-center">
                <input
                  className="shadow focus:shadow-lg duration-500 easy-in-out appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none text-center"
                  type="password"
                  placeholder="Confirm Password"
                  onChange={handleConfirmPasswordChange}
                  value={confirmPassword}
                />
              </div>
              <div className="grid grid-rows-1 content-center">
                <input
                  className="shadow focus:shadow-lg duration-500 easy-in-out appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none text-center"
                  type="text"
                  placeholder="Display Name"
                  onChange={handleDisplayName}
                />
              </div>
            </div>

            <div className="flex flex-col items-center">
              <button
                className="border-solid border rounded-lg border-blue-500 pt-1 pb-1 uppercase pl-24 pr-24 text-blue-500 hover:bg-blue-500 hover:text-white transition duration-500 ease-in-out"
                onClick={handleSignUp}
              >
                Sign Up
              </button>
            </div>
          </>
        )}
        <div className="flex flex-col items-center mt-5">
          <button
            className="border-none border rounded-lg border-blue-500 pt-1 pb-1 uppercase pl-24 pr-24 underline text-blue-500 hover:bg-blue-500 hover:text-white transition duration-500 ease-in-out"
            onClick={() => setLoggingIn(!loggingIn)}
          >
            {loggingIn ? "Sign Up" : "Sign In"}
          </button>
        </div>
      </div>
    </section>
  );
}
