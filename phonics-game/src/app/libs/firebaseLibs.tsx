import { db } from "@/app/firebaseInit";
import { ref, set } from "firebase/database";

/**
 * Saves the times of the selected level to the firebase database
 * @param userId - The user's id
 * @param level - The level to save
 * @param times - The times to save
 */

export const saveTime = (userId: string, level: string, times: number[]) => {
  const userRef = ref(db, `users/${userId}/times/${level}`);
  set(userRef, times);
};

/**
 * Writes the display name of the user to the firebase database
 * @param userId - The user's id
 * @param displayName - The display name to write
 */
export const writeDisplayName = (
  userId: string | undefined,
  displayName: string
) => {
  if (!userId) {
    console.log("Could not write display name");
    return;
  }
  const userRef = ref(db, `users/${userId}/name`);
  set(userRef, displayName);
};
