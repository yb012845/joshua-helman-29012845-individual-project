"use client";

import React, { useState } from "react";
import faqs from "@/app/help/faqs.json"; // Import the faqs.json file

import AuthChecker from "@/app/components/AuthChecker";
import { useRouter } from "next/navigation";
import BackButton from "@/app/components/BackButton";
import { styles } from "@/app/GlobalTailwindStyles";

import "./style.css";

// COMPONENTS //

export default function Learn() {
  const [loaded, setLoaded] = useState<boolean>(true);
  const [filter, setFilter] = useState<string>("");

  const router = useRouter();

  return (
    <main>
      <AuthChecker />
      <BackButton
        handlePress={() => {
          router.push("/dashboard");
        }}
      />

      <div className="flex flex-col items-center justify-center min-h-screen p-16">
        {loaded ? (
          <>
            <input
              placeholder="Search..."
              className="w-1/4 h-12 text-center border-blue-500 border-4 rounded-xl text-blue-500 mr-auto"
              onChange={(e) => {
                setFilter(e.target.value.toLowerCase());
              }}
            />
            <p className="" />
            <div className="flex flex-col items-center justify-center w-full py-2">
              {Object.entries(faqs).map(
                ([question, answer]) =>
                  (question.toLowerCase().match(filter) ||
                    answer.toLowerCase().match(filter)) && (
                    <div key={question} className="w-full">
                      <details className="">
                        <summary className="w-full cursor-pointer border-blue-500 rounded-xl">
                          <h3 className="text-blue-500">{question}</h3>
                        </summary>
                        <p className="p-4">{answer}</p>
                      </details>
                    </div>
                  )
              )}
            </div>
          </>
        ) : (
          <h1 className={styles.h1}>Loading...</h1>
        )}
      </div>
    </main>
  );
}
