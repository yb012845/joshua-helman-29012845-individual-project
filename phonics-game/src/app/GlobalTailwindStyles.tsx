export const styles = {
  button:
    "border-solid border rounded-lg border-blue-500 pt-1 pb-1 uppercase pl-24 pr-24 text-blue-500 hover:bg-blue-500 hover:text-white transition duration-500 ease-in-out bg-white",
  h1: "text-6xl font-bold text-center",
  cardStyle:
    "bg-white w-96 h-96 rounded-xl shadow-xl hover:shadow-2xl hover:scale-110 transition duration-500 ease-in-out content-center flex flex-col items-center justify-center m-4 cursor-pointer",
  levelCardStyle:
    "bg-white w-full h-52 rounded-xl shadow-xl hover:shadow-2xl hover:scale-110 transition duration-500 ease-in-out content-center flex flex-col items-center justify-center m-4 cursor-pointer",
};
