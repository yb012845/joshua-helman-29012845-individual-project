"use client";

// DEPENDENCIES //

let tf: any;
let speech: any;

import "@tensorflow/tfjs-backend-webgl";
import { SpeechCommandRecognizer } from "@tensorflow-models/speech-commands";

import React, { useEffect, useState } from "react";

import { levels, Level, Question } from "@/app/learn/questions";

import { styles } from "@/app/GlobalTailwindStyles";

import { useRouter } from "next/navigation";

import { saveTime } from "@/app/libs/firebaseLibs";
import { auth } from "../firebaseInit";

// COMPONENTS //

export default function PlayLevel({
  levelIndex,
  finishLevel,
}: {
  levelIndex: number;
  finishLevel: () => void;
}) {
  const [level, setLevel] = useState<Level>();
  const [question, setQuestion] = useState<Question | null>(null);
  const [questions, setQuestions] = useState<Question[] | null>(null);
  const [completed, setCompleted] = useState<boolean>(false);

  const [model, setModel] = useState<SpeechCommandRecognizer | null>(null);
  const [answer, setAnswer] = useState<string | null>(null);
  const [labels, setLabels] = useState<string[] | null>(null);
  const [isListening, setIsListening] = useState<boolean>(false);
  const [ready, setReady] = useState<boolean>(false);
  const [questionIndex, setQuestionIndex] = useState<number>(0);
  const [times, setTimes] = useState<number[]>([]);

  const [production, setProduction] = useState<boolean>(false);

  const [skip, setSkip] = useState<boolean>(false);

  /**
   * Load the TensorFlow.js model
   * @param model The model path to load
   */
  const loadModel = async (model: string) => {
    // Check if we're on the client side
    if (typeof window !== "undefined") {
      // Dynamically import TensorFlow.js
      tf = await import("@tensorflow/tfjs");
      speech = await import("@tensorflow-models/speech-commands");

      // Load the model
      const recognizer: SpeechCommandRecognizer = await speech.create(
        "BROWSER_FFT", //Fast Fourier Transform for audio processing
        undefined, // Model options
        `${
          production
            ? "https://fyp-psi-vert.vercel.app"
            : "http://localhost:3000"
        }/${model}/model.json`, // Model path
        `${
          production
            ? "https://fyp-psi-vert.vercel.app"
            : "http://localhost:3000"
        }/${model}/metadata.json` // Metadata path
      );

      await recognizer.ensureModelLoaded();

      console.log("Model Loaded");
      console.log(recognizer.wordLabels());

      setModel(recognizer);
      setLabels(recognizer.wordLabels());
      setReady(true);
    }
  };

  const skipQuestion = () => {
    model?.stopListening();
    setSkip(false);

    // Check if there are more questions
    if (questionIndex <= levels.length / 2 - 1) {
      setQuestionIndex(questionIndex + 1);
      setQuestion(null);
      setIsListening(false);
      times.push(100000);
    } else if (questionIndex >= levels.length / 2 - 1) {
      setQuestion(null);
      setCompleted(true);

      times.push(100000);
      console.log("TIMES: ", times);

      const userId = auth.currentUser?.uid || "";
      let timeSpent = 0;
      times.forEach((time) => (timeSpent += time));

      saveTime(userId, level?.getTitle() || "unknown-level", times);
      setIsListening(false);
    } else {
      // Finish the level
      setCompleted(true);
    }
  };

  useEffect(() => {
    // Set the level and questions at the beginning of the elements lifecycle. This runs every time the question changes also
    setLevel(levels[levelIndex]);
    setQuestions(levels[levelIndex].getQuestions());
    setQuestion(levels[levelIndex].getQuestions()[questionIndex]);

    loadModel(levels[levelIndex].getModel());

    setSkip(false);
  }, [question]);

  /**
   * Starts the model and listening for words
   */
  const startModel = async () => {
    if (model === null) {
      console.log("Model not loaded");
      return;
    }

    let startTime = Date.now();

    console.log("Listening for words");
    setIsListening(true);

    // Start listening for words
    model.listen(
      async (result) => {
        if (Date.now() - startTime > 30_000) setSkip(true);

        // Get the index of the word with the highest probability
        const scoresTensor = tf.tensor(Object.values(result.scores));
        const maxIndex = scoresTensor.argMax().dataSync()[0];

        // Get the word with the highest probability
        let temp = labels ? labels[maxIndex] : null;
        setAnswer(temp);
        console.log(result);

        // Check if the answer is correct
        if (temp == question?.getSound()) {
          console.log("Correct Word Said");

          // Completed Level when on the last question in the level, and it's completed
          if (
            questionIndex === (questions?.length ?? 0) - 1 &&
            question?.isFinished()
          ) {
            console.log("Completed Level");
            setQuestion(null);
            setCompleted(true);

            times.push(Date.now() - startTime);
            console.log("TIMES: ", times);

            const userId = auth.currentUser?.uid || "";
            let timeSpent = 0;
            times.forEach((time) => (timeSpent += time));

            saveTime(userId, level?.getTitle() || "unknown-level", times);

            model.stopListening();
            setIsListening(false);
          } else {
            // Completed Question

            if (question?.isFinished()) {
              console.log("Question Completed");
              times.push(Date.now() - startTime);
              console.log("TIMES: ", times);

              setQuestionIndex(questionIndex + 1);
              setQuestion(null);

              model.stopListening();
              setIsListening(false);
            } else {
              console.log("Stage Completed");
              question?.incrementStageIndex();
              setQuestionIndex(questionIndex);
              setQuestion(question);
            }
          }
        }
        return Promise.resolve();
      },
      {
        includeSpectrogram: true, // in case listen should return result.spectrogram
        probabilityThreshold: 0.5, // minimum probability to consider a word. Chosen optimally through experimentation.
        invokeCallbackOnNoiseAndUnknown: true, // must be true to invoke the callback.
        overlapFactor: 0.9, // Optimal overlap for the FFT window. Must be between 0 and 1. Default is 0.5.
      }
    );
  };

  return (
    <>
      {!completed ? (
        <div className="flex flex-col items-center justify-center py-2">
          {ready ? (
            <>
              <h1 className={styles.h1}>{question?.getTitle()}</h1>

              {question?.isMultiStage() ? (
                <p className="text-2xl">
                  {question?.getSounds().map((sound, index) => (
                    <>
                      <span
                        key={index}
                        className={
                          index === question?.getStageIndex()
                            ? "text-green-400 underline"
                            : ""
                        }
                      >
                        {sound}
                      </span>
                      <span> </span>
                    </>
                  ))}
                </p>
              ) : (
                <> </>
              )}

              {!isListening ? (
                <button onClick={startModel} className={styles.button}>
                  {"Are you ready?"}
                </button>
              ) : (
                <>
                  <p>Listening...</p>
                </>
              )}
              {skip ? (
                <div className="mt-8">
                  <p>Having trouble? You can skip the question!</p>
                  <button onClick={skipQuestion} className={styles.button}>
                    Skip Question
                  </button>
                </div>
              ) : (
                <></>
              )}
            </>
          ) : (
            <h1 className={styles.h1}>The model is loading. Please wait...</h1>
          )}
        </div>
      ) : (
        <>
          <div className="flex flex-col items-center justify-center py-2">
            <h1 className={styles.h1}>{"LEVEL COMPLETED"}</h1>
            <button onClick={finishLevel} className={styles.button}>
              {"Finish Level"}
            </button>
          </div>
        </>
      )}
    </>
  );
}
