export class Level {
  constructor(
    private title: string,
    private model: string,
    private questions: Question[],
    private dependent: string | null
  ) {}

  public getTitle(): string {
    return this.title;
  }

  public getModel(): string {
    return this.model;
  }

  public getQuestions(): Question[] {
    return this.questions;
  }

  public resetStage() {
    this.questions.forEach((question) => question.setStage(0));
  }

  public getDependent() {
    return this.dependent;
  }

  public getQuestionITitle(i: number) {
    return this.questions?.[i].getTitle();
  }
}

export class Question {
  constructor(
    protected title: string,
    private sound: string[],
    protected audioRecording: string | null,
    private stageIndex: number
  ) {}

  public getSound(): string {
    return this.sound[this.stageIndex];
  }

  public getSounds(): string[] {
    return this.sound;
  }

  public incrementStageIndex() {
    this.stageIndex++;
  }

  public getStageIndex(): number {
    return this.stageIndex;
  }

  public getTitle(): string {
    return this.title;
  }

  public getAudioRecording(): string | null {
    return this.audioRecording;
  }

  public isFinished(): boolean {
    return this.stageIndex >= this.sound.length - 1;
  }

  public setStage(stage: number) {
    this.stageIndex = stage;
  }

  public isMultiStage(): boolean {
    return this.sound.length > 1;
  }
}

export const levels = [
  new Level(
    "Level 1 - S, A, T, P", // Title
    "SATP", // Model
    [
      new Question("What does the 's' make in 'sound'?", ["s"], null, 0), // Question 1
      new Question("What does the 'a' make in 'can'", ["a"], null, 0), // Question 2
      new Question("What does the 't' make in 'train'?", ["t"], null, 0), // Question 3
      new Question("What does the 'p' make in 'plant'?", ["p"], null, 0), // Question 4
      // Add more questions here...
    ],
    null // Dependent
  ),
  new Level(
    "Level 2 - I, N, M, D",
    "INMD",
    [
      new Question("What does the 'i' make in 'pip'?", ["i"], null, 0),
      new Question("What does the 'n' make in 'grandma'?", ["n"], null, 0),
      new Question("What does the 'm' make in 'map'?", ["m"], null, 0),
      new Question("What does the 'd' make in 'dog'?", ["d"], null, 0),
    ],
    "Level 1 - S, A, T, P"
  ),
  new Level(
    "Level 3 - G, O, C, K",
    "GOCK",
    [
      new Question("What does the 'g' make in 'game'?", ["g"], null, 0),
      new Question("What does the 'o' make in 'octopus'?", ["o"], null, 0),
      new Question("What does the 'c' make in 'circle'?", ["c"], null, 0),
      new Question("What does the 'k' make in 'kick'?", ["k"], null, 0),
      new Question("What does the 'c' make in 'carrot'?", ["k"], null, 0),
    ],
    "Level 2 - I, N, M, D"
  ),
  new Level(
    "Level 4 - CK, E, U, R",
    "CKEUR",
    [
      new Question("What does the 'ck' make in 'duck'?", ["k"], null, 0),
      new Question("What does the 'e' make in 'elephant?", ["e"], null, 0),
      new Question("What does the 'u' make in 'umbrella'?", ["u"], null, 0),
      new Question("What does the 'r' make in 'rake'?", ["r"], null, 0),
    ],
    "Level 3 - G, O, C, K"
  ),
  new Level(
    "Level 5 - Words 1",
    "words1",
    [
      new Question("How do you say 'like'?", ["like"], null, 0),
      new Question("How do you say 'some'?", ["some"], null, 0),
      new Question("How do you say 'come'?", ["come"], null, 0),
      new Question("How do you say 'said'?", ["said"], null, 0),
      new Question("How do you say 'so'?", ["so"], null, 0),
      new Question("How do you say 'have'?", ["have"], null, 0),
    ],
    "Level 4 - CK, E, U, R"
  ),
  new Level(
    "Level 6 - Words 2",
    "words2",
    [
      new Question("How do you say 'he'?", ["he"], null, 0),
      new Question("How do you say 'she'?", ["she"], null, 0),
      new Question("How do you say 'me'?", ["me"], null, 0),
      new Question("How do you say 'we'?", ["we"], null, 0),
      new Question("How do you say 'be'?", ["be"], null, 0),
      new Question("How do you say 'was'?", ["was"], null, 0),
    ],
    "Level 5 - Words 1"
  ),
  // new Level(
  //   "Level 7 - Sentence 1",
  //   "words1",
  //   [
  //     new Question(
  //       "Read the following sentence: ",
  //       ["like", "some", "come", "said", "so", "have"],
  //       null,
  //       0
  //     ),
  //   ],
  //   "Level 6 - Words 2"
  // ),
  new Level(
    "Level 7 - Sentence 1",
    "sentence1",
    [
      new Question(
        "Read the following sentence: ",
        ["the", "lazy", "dog", "jumps", "over", "the", "quick", "brown", "fox"],
        null,
        0
      ),
    ],
    "Level 6 - Words 2"
  ),
];
