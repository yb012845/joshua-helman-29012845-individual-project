"use client";

// DEPENDENCIES //

import React, { useEffect, useState } from "react";

import AuthChecker from "@/app/components/AuthChecker";

import BackButton from "@/app/components/BackButton";
import PlayLevel from "@/app/learn/PlayLevel";

import { levels } from "@/app/learn/questions";
import { useRouter } from "next/navigation";

import { auth, db } from "../firebaseInit";
import { get, child, ref } from "firebase/database";

import { styles } from "@/app/GlobalTailwindStyles";

// COMPONENTS //

export default function Learn() {
  const [level, setLevel] = useState<number>(-1);
  const [times, setTimes] = useState<number[]>([]);
  const [loaded, setLoaded] = useState<boolean>(false);
  const [timesDictionary, setTimesDictionary] = useState();

  const router = useRouter();

  const selectLevel = (index: number) => () => {
    setLevel(index);
    levels[index].resetStage();
  };

  const finishLevel = () => {
    setLevel(-1);
  };

  // Runs on page load and when level changes
  useEffect(() => {
    // Check if user is logged in
    auth.onAuthStateChanged((user) => {
      // Get the times from the database
      const dbRef = ref(db);
      get(child(dbRef, `users/${auth.currentUser?.uid}/times`))
        .then((snapshot) => {
          const tmp = snapshot.val();

          setTimesDictionary(tmp);

          setLoaded(true);
        })
        .catch((error) => {
          console.error(error);
        });
    });
  }, [level, setLevel]);

  const addTimes = (times: number[]) => {
    let total = 0;
    times.forEach((t) => {
      total += t;
    });
    return total;
  };

  return (
    <main>
      <AuthChecker />
      <BackButton
        handlePress={() => {
          if (level > -1) setLevel(-1);
          else router.push("/dashboard");
        }}
      />

      <div className="flex flex-col items-center justify-center min-h-screen p-16">
        {loaded ? (
          <>
            {level > -1 ? (
              <PlayLevel levelIndex={level} finishLevel={finishLevel} />
            ) : (
              <div className="grid sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4">
                {levels.map((level, index) => (
                  <div
                    key={index}
                    className={styles.levelCardStyle}
                    onClick={
                      level.getDependent() == null ||
                      timesDictionary?.[level.getDependent() || ""]
                        ? selectLevel(index)
                        : () => {
                            alert(
                              "This level cannot be played just. Yet, you must complete '" +
                                level.getDependent() +
                                "' before playing this level."
                            );
                          }
                    }
                  >
                    <p className="text-black">
                      {level.getTitle() +
                        " | " +
                        (addTimes(timesDictionary?.[level.getTitle()] || [-1]) >
                        0
                          ? "Complete"
                          : "Incomplete")}
                    </p>
                  </div>
                ))}
              </div>
            )}
          </>
        ) : (
          <h1 className={styles.h1}>Loading...</h1>
        )}
      </div>
    </main>
  );
}
