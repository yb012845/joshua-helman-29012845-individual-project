# Phonics Game Installation Instructions

To build and run this application, you must be running Node.js v21 or higher, and NPM version 10 or higher.
- Install Node.js from [here](https://nodejs.org/en/download/)

To build and run this application, you must first download the source code from the GitHub repository. You can do this by running the following command in your terminal:
- `git clone https://csgitlab.reading.ac.uk/yb012845/joshua-helman-29012845-individual-project.git`

Once the source code is downloaded, you must run the following commands in the `phonics-game` directory:
- `cd phonics-game` - Change directory to the root of the project
- `npm install --legacy-peer-deps` - Install the dependencies for this application using compatible versions for all dependencies
- `npm run build` - Build the application
- `npm run start` - Run the application

The test account login details are:
- Username: test@test.com
- Password: test@test.com

You may also create your own account with the built-in sign-up feature!

A public version of the application is available at [https://fyp-psi-vert.vercel.app](https://fyp-psi-vert.vercel.app)