import pyaudio
import wave
import os


def record_audio(file_name, duration=1, chunk=1024, channels=1, rate=44100):
    audio = pyaudio.PyAudio()
    stream = audio.open(format=pyaudio.paInt16, channels=channels,
                        rate=rate, input=True,
                        frames_per_buffer=chunk)
    input("Press enter to start recording.")
    print("Recording...")
    frames = []
    for i in range(0, int(rate / chunk * duration)):
        data = stream.read(chunk)
        frames.append(data)
    print("Finished recording.")
    stream.stop_stream()
    stream.close()
    audio.terminate()

    wf = wave.open(file_name, 'wb')
    wf.setnchannels(channels)
    wf.setsampwidth(audio.get_sample_size(pyaudio.paInt16))
    wf.setframerate(rate)
    wf.writeframes(b''.join(frames))
    wf.close()


if __name__ == "__main__":
    name = input("Enter the word name: ")
    output_folder = f"audio_samples/{name}"
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    # Number of samples to record
    num_samples = int(input("Number of Samples: "))
    num_offset = int(input("Offset: "))

    for i in range(num_samples):
        file_name = os.path.join(
            output_folder, f"{i+1 + num_offset}.wav")
        record_audio(file_name)
